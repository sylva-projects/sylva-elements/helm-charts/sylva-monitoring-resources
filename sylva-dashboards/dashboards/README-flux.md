Flux dashboards are based on following example:

https://github.com/fluxcd/flux2-monitoring-example/tree/main/monitoring/configs/dashboards

Some modifications were made in order to be compliant with our exposed metrics for flux resources.
